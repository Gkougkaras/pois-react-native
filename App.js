import * as React from 'react';
import { View, Text, Button , StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { MaterialCommunityIcons } from 'react-native-vector-icons';
import GetLocation  from 'react-native-geolocation-service';

function Separator() {
  return <View style={styles.separator} />;
}

const styles = StyleSheet.create({
  lightBlue: {
    color: 'rgb(0,255,200)',
    fontWeight: 'bold',
    fontSize: 30,
  },
   separator: {
    marginVertical: 8,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

function HelloScreen({ navigation }) {
  return (
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center" }}>
      <Text style={styles.lightBlue}>Welcome!!</Text>
      <Separator />
      <Button
        title="Join Us!!"
        color="#f0150a"
        onPress={() =>
          navigation.navigate('MyTabs', { name: 'Home' })
        }
      />
    </View>
  );
}

function Home({ navigation }) {
  return (
     <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center" }}>
     <Button title="Go Back" color="#f0150a" onPress={() => navigation.navigate('Hello')} />
    </View>
  );
}

function Maps() {
  return (
     <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center" }}>
      <Text>Profile!</Text>
    </View>
  );
}

function List() {
  return (
     <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.8)', justifyContent: "center", alignItems: "center" }}>
      <Text>List!</Text>
    </View>
  );
}


const Tab = createMaterialBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#e91e63"
      labelStyle={{ fontSize: 12 }}
      style={{ backgroundColor: 'tomato' }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="List"
        component={List}
        options={{
          tabBarLabel: 'List',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="format-list-bulleted" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Maps"
        component={Maps}
        options={{
          tabBarLabel: 'Maps',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="earth" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

 function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName="Hello"
      screenOptions={{
    headerShown: false
  }}>
        <Stack.Screen
          name="Hello"
          component={HelloScreen}
           
        />
        <Stack.Screen
          name="MyTabs"
          component={MyTabs}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;