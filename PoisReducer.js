import { combineReducers } from 'redux';

const INITIAL_STATE = {
  current: [],
  possible: [
    Yes
  ],
};

const PoisReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state
  }
};

export default combineReducers({
  Pois: PoisReducer,
});