# POIs React-Native

Creating a 2 screen react-native app able to sort and manage points of interest on maps.

The first screen is a welcome page asking for location permission when it starts and navigating to the second screen.

The second screen has one bottom tab navigator with two tabs(list and maps). The first tab is responsible for fetching data  from a redux store containing points of interest
and displays them by distance. In case the location permission was not granted it sorts them alphabetically.
The Maps tab is resposible for showing those POIs in cluster form on a map.